FROM alpine:latest

LABEL description="Docker image with curl and unzip"
LABEL maintainer="Nicolas Spath <nicolas.spath@web.de>"

RUN apk add --no-cache curl unzip